# Mod for adding modding/patching support to android renpy

This mod adds modding support to renpy android because the current engine doesn't support it.


## Getting started

First open up the android mod/renpy folder.

Secondly open up main.py.

And lastly go to line 262 and change the string `booom313_android_patch_support` to the foldername you want the game to have.



## Installing

Copy the edited renpy folder to the renpy sdk and drop it there.

It'll ask to overwrite choose `Yes`.

Tada you've added modding/patching support to android!



## Caution

This current release doesn't allow for taking priority over the game folder.


For example:

you've a picture located in `game/image/apple.png`

and you've in your mod/patch a picture located in `modfolder/image/apple.png`

It'll automaticly choose the first one and ignore the modded one.


*So make sure it isn't saved at the same location + name as in the normal gamefolder*


## faq

> Where is the mod folder located?

It's located in `storage/emulated/0/(mod_folder)` where mod_folder is the name you've given it


> Will this affect my normal pc release?

No, it should not activate if the device is not android so your pc build will be unaffected by this change.


## contact or support

I'm almost always online on discord so shoot me a pm there.

discord username : booom313#0313

[My discord (18+)](https://discord.gg/tpBn23f)


### This project is licensed under 

This project is licensed under the WTFPL License - see the [LICENSE.md](LICENSE.md) file for details


## Made by booom313.


## And a huge shoutout to renpytom for creating such a amazing framework.